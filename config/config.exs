# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :budget_hub,
  ecto_repos: [BudgetHub.Repo]

# Configures the endpoint
config :budget_hub, BudgetHubWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "hiIZNm10M66xqL8qrIuvGIV3tA/n/QPkmE3DmoR7DTR56Q+mhOWDUJFV08C930dY",
  render_errors: [view: BudgetHubWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: BudgetHub.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix and Ecto
config :phoenix, :json_library, Jason
config :ecto, :json_library, Jason

config :budget_hub, BudgetHub.Guardian,
  issuer: "budget_hub",
  secret_key: "W8bFWCM8zApx8U8tU+TfeprLySVUAAY6tHzUYH0hdw00HsIhvhYPpcnf91xipasl"

config :cors_plug,
  expose: [
    "Authorization"
  ]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
