defmodule BudgetHub.GuardianTest do
  use BudgetHub.DataCase

  alias BudgetHub.Guardian

  describe "subject_for_token/1" do
    test "with valid %User{} record returns User:<id>" do
      user = user_fixture()
      assert Guardian.subject_for_token(user, nil) == {:ok, "User:#{user.id}"}
    end

    test "with unknown resource returns error" do
      assert Guardian.subject_for_token(%{some: "thing"}, nil) == {:error, :unknown_resource}
    end
  end

  describe "resource_from_claims/1" do
    test "with valid User ID returns {:ok, %User{}}" do
      user = user_fixture()
      user = %{user | password: nil}

      assert Guardian.resource_from_claims(%{"sub" => "User:" <> to_string(user.id)}) ==
               {:ok, user}
    end

    test "with invalid ID returns {:error, :invalid_id}" do
      assert Guardian.resource_from_claims(%{"sub" => "User:a1b2c3"}) == {:error, :invalid_id}
      assert Guardian.resource_from_claims(%{"sub" => "User:abc123"}) == {:error, :invalid_id}
      assert Guardian.resource_from_claims(%{"sub" => "User:"}) == {:error, :invalid_id}
    end

    test "with non-existent ID returns {:error, :no_result}" do
      assert Guardian.resource_from_claims(%{"sub" => "User:99999"}) == {:error, :no_result}
    end

    test "with invalid object returns {:error, :invalid_claims}" do
      assert Guardian.resource_from_claims(%{"sub" => "UnknownObject:123"}) ==
               {:error, :invalid_claims}
    end
  end
end
