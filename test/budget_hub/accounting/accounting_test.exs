defmodule BudgetHub.AccountingTest do
  use BudgetHub.DataCase

  alias BudgetHub.Accounting

  describe "accounts" do
    alias BudgetHub.Accounting.Account

    @valid_attrs %{
      name: "Sample Account",
      type: "asset",
      contra: false
    }
    @update_attrs %{
      name: "Updated Sample Account",
      type: "liability",
      contra: true
    }
    @invalid_attrs %{
      name: nil,
      type: nil,
      contra: nil
    }

    test "list_accounts/0 returns all accounts" do
      account = account_fixture()
      assert Accounting.list_accounts() == [account]
    end

    test "get_account!/1 returns the account with given id" do
      account = account_fixture()
      assert Accounting.get_account!(account.id) == account
    end

    test "get_account/1 returns the account with given id" do
      account = account_fixture()
      assert Accounting.get_account(account.id) == account
    end

    test "create_account/1 with valid data creates a account" do
      assert {:ok, %Account{} = account} = Accounting.create_account(@valid_attrs)
      assert account.name == "Sample Account"
      assert account.type == "asset"
      assert account.contra == false
    end

    test "create_account/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounting.create_account(@invalid_attrs)
    end

    test "update_account/2 with valid data updates the account" do
      account = account_fixture()
      assert {:ok, %Account{} = account} = Accounting.update_account(account, @update_attrs)

      assert account.name == "Updated Sample Account"
      assert account.type == "liability"
      assert account.contra == true
    end

    test "update_account/2 with invalid data returns error changeset" do
      account = account_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounting.update_account(account, @invalid_attrs)
      assert account == Accounting.get_account!(account.id)
    end

    test "delete_account/1 deletes the account" do
      account = account_fixture()
      assert {:ok, %Account{}} = Accounting.delete_account(account)
      assert_raise Ecto.NoResultsError, fn -> Accounting.get_account!(account.id) end
    end

    test "change_account/1 returns a account changeset" do
      account = account_fixture()
      assert %Ecto.Changeset{} = Accounting.change_account(account)
    end
  end

  describe "entries" do
    alias BudgetHub.Accounting.Entry

    @valid_attrs %{date: ~D[2010-04-17], description: "Sample Entry"}
    @update_attrs %{date: ~D[2011-05-18], description: "Updated Sample Entry"}
    @invalid_attrs %{date: nil, description: nil}

    test "list_entries/0 returns all entries" do
      entry = entry_fixture()
      assert Accounting.list_entries() == [entry]
    end

    test "get_entry!/1 returns the entry with given id" do
      entry = entry_fixture()
      assert Accounting.get_entry!(entry.id) == entry
    end

    test "get_entry/1 with valid id returns Entry" do
      entry = entry_fixture()
      assert Accounting.get_entry(entry.id) == entry
    end

    test "get_entry/1 with invalid id returns Entry" do
      entry = entry_fixture()
      assert Accounting.get_entry(entry.id + 1) == {:error, :not_found}
    end

    test "create_entry/1 with valid data creates a entry" do
      assert {:ok, %Entry{} = entry} = Accounting.create_entry(@valid_attrs)
      assert entry.date == ~D[2010-04-17]
      assert entry.description == "Sample Entry"
    end

    test "create_entry/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounting.create_entry(@invalid_attrs)
    end

    test "update_entry/2 with valid data updates the entry" do
      entry = entry_fixture()
      assert {:ok, %Entry{} = entry} = Accounting.update_entry(entry, @update_attrs)

      assert entry.date == ~D[2011-05-18]
      assert entry.description == "Updated Sample Entry"
    end

    test "update_entry/2 with invalid data returns error changeset" do
      entry = entry_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounting.update_entry(entry, @invalid_attrs)
      assert entry == Accounting.get_entry!(entry.id)
    end

    test "delete_entry/1 deletes the entry" do
      entry = entry_fixture()
      assert {:ok, %Entry{}} = Accounting.delete_entry(entry)
      assert_raise Ecto.NoResultsError, fn -> Accounting.get_entry!(entry.id) end
    end

    test "change_entry/1 returns a entry changeset" do
      entry = entry_fixture()
      assert %Ecto.Changeset{} = Accounting.change_entry(entry)
    end
  end

  describe "amounts" do
    alias BudgetHub.Accounting.Amount

    @update_attrs %{amount: "456.7", type: "credit"}
    @invalid_attrs %{amount: nil, type: nil}

    test "list_amounts/0 returns all amounts" do
      amount_fixture()
      assert length(Accounting.list_amounts()) == 1
    end

    test "get_amount!/1 returns the amount with given id" do
      amount = amount_fixture()
      assert Accounting.get_amount!(amount.id) == amount
    end

    test "get_amount/1 returns the amount with given id" do
      amount = amount_fixture()
      assert Accounting.get_amount(amount.id) == amount
    end

    test "get_amount/1 with invalid id returns error" do
      amount = amount_fixture()
      assert Accounting.get_amount(amount.id + 1) == {:error, :not_found}
    end

    test "create_amount/1 with valid data creates a amount" do
      attrs = %{
        amount: "120.5",
        type: "debit",
        account_id: account_fixture().id,
        entry_id: entry_fixture().id
      }

      assert {:ok, %Amount{} = amount} = Accounting.create_amount(attrs)
      assert amount.amount == Decimal.new("120.5")
      assert amount.type == "debit"
    end

    test "create_amount/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounting.create_amount(@invalid_attrs)
    end

    test "update_amount/2 with valid data updates the amount" do
      amount = amount_fixture()
      assert {:ok, %Amount{} = amount} = Accounting.update_amount(amount, @update_attrs)

      assert amount.amount == Decimal.new("456.7")
      assert amount.type == "credit"
    end

    test "update_amount/2 with invalid data returns error changeset" do
      amount = amount_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounting.update_amount(amount, @invalid_attrs)
      assert amount == Accounting.get_amount!(amount.id)
    end

    test "delete_amount/1 deletes the amount" do
      amount = amount_fixture()
      assert {:ok, %Amount{}} = Accounting.delete_amount(amount)
      assert_raise Ecto.NoResultsError, fn -> Accounting.get_amount!(amount.id) end
    end

    test "change_amount/1 returns a amount changeset" do
      amount = amount_fixture()
      assert %Ecto.Changeset{} = Accounting.change_amount(amount)
    end
  end
end
