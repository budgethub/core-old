defmodule BudgetHub.AuthenticationTest do
  use BudgetHub.DataCase

  alias BudgetHub.Authentication

  describe "users" do
    alias BudgetHub.Authentication.User

    @valid_attrs %{
      first_name: "Test",
      last_name: "User",
      password: "password",
      password_confirmation: "password",
      username: "testuser"
    }
    @update_attrs %{
      first_name: "Updated Test",
      last_name: "Updated User"
    }
    @invalid_attrs %{
      first_name: nil,
      last_name: nil,
      password: nil,
      username: nil
    }

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Authentication.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Authentication.get_user!(user.id) == user
    end

    test "get_user/1 returns the user with given id" do
      user = user_fixture()
      assert Authentication.get_user(user.id) == user
    end

    test "get_user_by/1 returns the user with the given map" do
      user = user_fixture(%{username: "hellouser"})
      assert Authentication.get_user_by(%{username: "hellouser"}) == user
    end

    test "get_user_by/1 with non-matching username returns error" do
      assert Authentication.get_user_by(%{username: "exampleuser"}) == {:error, :not_found}
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Authentication.create_user(@valid_attrs)
      assert user.first_name == "Test"
      assert user.last_name == "User"
      assert user.password_hash != ""
      assert user.username == "testuser"
    end

    test "create_user/1 with non-unique user returns error changeset" do
      user = user_fixture()

      attrs = %{@valid_attrs | username: user.username}

      {:error, %Ecto.Changeset{} = changeset} = Authentication.create_user(attrs)

      assert changeset.errors[:username] == {"has already been taken", []}
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Authentication.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Authentication.update_user(user, @update_attrs)

      assert user.first_name == "Updated Test"
      assert user.last_name == "Updated User"
      assert user.password_hash != ""
      assert user.username != "testuser"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Authentication.update_user(user, @invalid_attrs)
      assert user == Authentication.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Authentication.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Authentication.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Authentication.change_user(user)
    end

    test "authenticate_user/1 with valid credentials returns user" do
      user = user_fixture(%{username: "exampleuser"})

      {:ok, %User{} = authuser} =
        Authentication.authenticate_user(%{"username" => "exampleuser", "password" => "password"})

      assert authuser == user
    end

    test "authenticate_user/1 with invalid credentials returns error" do
      assert {:error, :not_found} ==
               Authentication.authenticate_user(%{
                 "username" => "ghostuser",
                 "password" => "password"
               })
    end
  end
end
