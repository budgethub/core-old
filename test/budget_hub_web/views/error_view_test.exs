defmodule BudgetHubWeb.ErrorViewTest do
  use BudgetHubWeb.ConnCase, async: true

  # Bring render/3 and render_to_string/3 for testing custom views
  import Phoenix.View

  test "renders 404.json" do
    assert render(BudgetHubWeb.ErrorView, "404.json", []) == %{
             errors: [%{code: 404, message: "Resource Not Found"}]
           }
  end

  test "renders 500.json" do
    assert render(BudgetHubWeb.ErrorView, "500.json", []) ==
             %{errors: %{detail: "Internal Server Error"}}
  end
end
