defmodule BudgetHubWeb.AccountControllerTest do
  use BudgetHubWeb.ConnCase

  alias BudgetHub.Accounting.Account

  @create_attrs %{
    name: "Sample Account",
    type: "asset",
    contra: false
  }
  @update_attrs %{
    name: "Updated Sample Account",
    type: "liability",
    contra: true
  }
  @invalid_attrs %{
    name: nil,
    type: nil,
    contra: nil
  }

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    setup [:create_account]

    test "lists all accounts", %{conn: conn, account: %Account{id: id}} do
      conn = get(conn, Routes.account_path(conn, :index))

      expected_response = %{
        "id" => id,
        "name" => "Sample Account",
        "type" => "asset",
        "contra" => false
      }

      assert json_response(conn, 200) == [expected_response]
    end
  end

  describe "#show" do
    setup [:create_account]

    test "with valid id returns account", %{conn: conn, account: %Account{id: id}} do
      response =
        conn
        |> get(Routes.account_path(conn, :show, id))
        |> json_response(200)

      expected_response = %{
        "id" => id,
        "name" => "Sample Account",
        "type" => "asset",
        "contra" => false
      }

      assert expected_response == response
    end

    test "with non-existent id returns error", %{conn: conn, account: %Account{id: id}} do
      response =
        conn
        |> get(Routes.account_path(conn, :show, id + 1))
        |> json_response(404)

      error = Enum.at(response["errors"], 0)
      assert 404 == error["code"]
      assert "Resource Not Found" == error["message"]
    end
  end

  describe "create account" do
    test "renders account when data is valid", %{conn: conn} do
      response =
        conn
        |> post(Routes.account_path(conn, :create), @create_attrs)
        |> json_response(201)

      assert "" != response["id"]
      assert "Sample Account" == response["name"]
      assert "asset" == response["type"]
      refute response["contra"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      response =
        conn
        |> post(Routes.account_path(conn, :create), @invalid_attrs)
        |> json_response(422)

      assert ["can't be blank"] == response["errors"]["name"]
      assert ["can't be blank"] == response["errors"]["type"]
      assert ["can't be blank"] == response["errors"]["contra"]
    end
  end

  describe "update account" do
    setup [:create_account]

    test "renders account when data is valid", %{conn: conn, account: %Account{id: id} = account} do
      response =
        conn
        |> put(Routes.account_path(conn, :update, account), @update_attrs)
        |> json_response(200)

      assert id == response["id"]
      assert "Updated Sample Account" == response["name"]
      assert "liability" == response["type"]
      assert response["type"]
    end

    test "renders errors when data is invalid", %{conn: conn, account: account} do
      response =
        conn
        |> put(Routes.account_path(conn, :update, account), @invalid_attrs)
        |> json_response(422)

      assert ["can't be blank"] == response["errors"]["name"]
      assert ["can't be blank"] == response["errors"]["type"]
      assert ["can't be blank"] == response["errors"]["contra"]
    end
  end

  describe "delete account" do
    setup [:create_account]

    test "deletes chosen account", %{conn: conn, account: account} do
      conn = delete(conn, Routes.account_path(conn, :delete, account))
      assert response(conn, 204)

      response =
        conn
        |> get(Routes.account_path(conn, :show, account))
        |> json_response(404)

      error = Enum.at(response["errors"], 0)
      assert 404 == error["code"]
      assert "Resource Not Found" == error["message"]
    end
  end

  defp create_account(_) do
    account = account_fixture()
    {:ok, account: account}
  end
end
