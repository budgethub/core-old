defmodule BudgetHubWeb.UserControllerTest do
  use BudgetHubWeb.ConnCase

  @create_attrs %{
    first_name: "Test",
    last_name: "User",
    password: "password",
    password_confirmation: "password",
    username: "testuser"
  }
  @invalid_attrs %{first_name: nil, last_name: nil, password: nil, username: nil}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "create user" do
    setup [:create_user]

    test "renders user when data is valid", %{conn: conn} do
      response =
        conn
        |> post(Routes.user_path(conn, :create), @create_attrs)
        |> json_response(201)

      assert response["first_name"] == "Test"
      assert response["last_name"] == "User"
      assert response["username"] == "testuser"
    end

    test "returns Authorization header when data is valid", %{conn: conn} do
      conn = post(conn, Routes.user_path(conn, :create), @create_attrs)

      {"authorization", token} =
        Enum.find(conn.resp_headers, fn element -> match?({"authorization", _}, element) end)

      assert token =~ "Bearer "
    end

    test "renders error when data includes non-unique username", %{conn: conn, user: user} do
      attrs = %{@create_attrs | username: user.username}

      response =
        conn
        |> post(Routes.user_path(conn, :create), attrs)
        |> json_response(422)

      assert response["errors"]["username"] == ["has already been taken"]
    end

    test "renders error when data includes non-matching passwords", %{conn: conn} do
      attrs = %{@create_attrs | password_confirmation: "differentpassword"}

      response =
        conn
        |> post(Routes.user_path(conn, :create), attrs)
        |> json_response(422)

      assert response["errors"]["password_confirmation"] == ["does not match confirmation"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      response =
        conn
        |> post(Routes.user_path(conn, :create), @invalid_attrs)
        |> json_response(422)

      assert response["errors"]["first_name"] == ["can't be blank"]
      assert response["errors"]["last_name"] == ["can't be blank"]
      assert response["errors"]["password"] == ["can't be blank"]
      assert response["errors"]["username"] == ["can't be blank"]
    end
  end

  defp create_user(_) do
    user = user_fixture()
    {:ok, user: user}
  end
end
