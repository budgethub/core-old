defmodule BudgetHubWeb.EntryControllerTest do
  use BudgetHubWeb.ConnCase

  alias BudgetHub.Accounting.Entry

  @create_attrs %{
    date: ~D[2010-04-17],
    description: "Sample Entry"
  }
  @update_attrs %{
    date: ~D[2011-05-18],
    description: "Updated Sample Entry"
  }
  @invalid_attrs %{date: nil, description: nil}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    setup [:create_entry]

    test "lists all entries", %{conn: conn} do
      api_response =
        conn
        |> get(Routes.entry_path(conn, :index))
        |> json_response(200)
        |> Enum.at(0)

      assert api_response["date"] == Date.utc_today() |> Date.to_string()
      assert api_response["description"] == "Sample Entry"
    end
  end

  describe "show" do
    setup [:create_entry]

    test "with valid id returns entry", %{conn: conn, entry: entry} do
      api_response =
        conn
        |> get(Routes.entry_path(conn, :show, entry))
        |> json_response(200)

      assert api_response["date"] == Date.utc_today() |> Date.to_string()
      assert api_response["description"] == "Sample Entry"
    end
  end

  describe "create entry" do
    test "renders entry when data is valid", %{conn: conn} do
      api_response =
        conn
        |> post(Routes.entry_path(conn, :create), @create_attrs)
        |> json_response(201)

      assert api_response["description"] == "Sample Entry"
      assert api_response["date"] == "2010-04-17"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      api_response =
        conn
        |> post(Routes.entry_path(conn, :create), @invalid_attrs)
        |> json_response(422)

      expected_response = %{
        "errors" => %{
          "date" => ["can't be blank"],
          "description" => ["can't be blank"]
        }
      }

      assert expected_response == api_response
    end
  end

  describe "update entry" do
    setup [:create_entry]

    test "renders entry when data is valid", %{conn: conn, entry: %Entry{id: id} = entry} do
      api_response =
        conn
        |> put(Routes.entry_path(conn, :update, entry), @update_attrs)
        |> json_response(200)

      assert api_response["id"] == id
      assert api_response["date"] == "2011-05-18"
      assert api_response["description"] == "Updated Sample Entry"
    end

    test "renders errors when data is invalid", %{conn: conn, entry: entry} do
      api_response =
        conn
        |> put(Routes.entry_path(conn, :update, entry), @invalid_attrs)
        |> json_response(422)

      expected_response = %{
        "errors" => %{
          "date" => ["can't be blank"],
          "description" => ["can't be blank"]
        }
      }

      assert expected_response == api_response
    end
  end

  describe "delete entry" do
    setup [:create_entry]

    test "deletes chosen entry", %{conn: conn, entry: entry} do
      conn = delete(conn, Routes.entry_path(conn, :delete, entry))
      assert response(conn, 204)

      response =
        conn
        |> get(Routes.entry_path(conn, :show, entry))
        |> json_response(404)

      error = Enum.at(response["errors"], 0)
      assert 404 == error["code"]
      assert "Resource Not Found" == error["message"]
    end
  end

  defp create_entry(_) do
    entry = entry_fixture()
    {:ok, entry: entry}
  end
end
