defmodule BudgetHubWeb.TokenControllerTest do
  use BudgetHubWeb.ConnCase

  @create_attrs %{
    password: "password",
    username: "testuser"
  }
  @invalid_attrs %{password: "", username: ""}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "create" do
    setup [:create_user]

    test "renders user when authentication is successful", %{conn: conn} do
      response =
        conn
        |> post(Routes.token_path(conn, :create), @create_attrs)
        |> json_response(201)

      assert response["first_name"] == "Test"
      assert response["last_name"] == "User"
      assert response["username"] == "testuser"
    end

    test "returns Authorization header when authentication is successful", %{conn: conn} do
      conn = post(conn, Routes.token_path(conn, :create), @create_attrs)

      {"authorization", token} =
        Enum.find(conn.resp_headers, fn element -> match?({"authorization", _}, element) end)

      assert token =~ "Bearer "
    end

    test "renders error when data includes invalid username", %{conn: conn} do
      attrs = %{@create_attrs | username: "invaliduser"}

      response =
        conn
        |> post(Routes.token_path(conn, :create), attrs)
        |> json_response(404)

      expected_response = %{
        "errors" => [
          %{
            "code" => 404,
            "message" => "Resource Not Found"
          }
        ]
      }

      assert response == expected_response
    end

    test "renders error when data includes invalid password", %{conn: conn} do
      attrs = %{@create_attrs | password: "differentpassword"}

      response =
        conn
        |> post(Routes.token_path(conn, :create), attrs)
        |> json_response(404)

      expected_response = %{
        "errors" => [
          %{
            "code" => 404,
            "message" => "Resource Not Found"
          }
        ]
      }

      assert response == expected_response
    end

    test "renders errors when data is invalid", %{conn: conn} do
      response =
        conn
        |> post(Routes.token_path(conn, :create), @invalid_attrs)
        |> json_response(404)

      expected_response = %{
        "errors" => [
          %{
            "code" => 404,
            "message" => "Resource Not Found"
          }
        ]
      }

      assert response == expected_response
    end
  end

  defp create_user(_) do
    user = user_fixture(%{username: "testuser"})
    {:ok, user: user}
  end
end
