defmodule BudgetHub.Factories do
  alias BudgetHub.Accounting
  alias BudgetHub.Authentication
  alias BudgetHub.Repo

  @valid_account_attrs %{
    name: "Sample Account",
    type: "asset",
    contra: false
  }

  def account_fixture(attrs \\ %{}) do
    {:ok, account} =
      attrs
      |> Enum.into(@valid_account_attrs)
      |> Accounting.create_account()

    account
  end

  def user_fixture(attrs \\ %{}) do
    username = "user#{System.unique_integer([:positive])}"

    {:ok, user} =
      attrs
      |> Enum.into(%{
        username: username,
        first_name: "Test",
        last_name: "User",
        password: "password",
        password_confirmation: "password"
      })
      |> Authentication.create_user()

    %{user | password: nil}
  end

  def entry_fixture(attrs \\ %{}) do
    {:ok, entry} =
      attrs
      |> Enum.into(%{
        description: "Sample Entry",
        date: Date.utc_today()
      })
      |> Accounting.create_entry()

    entry
  end

  def amount_fixture(attrs \\ %{}) do
    {:ok, amount} =
      attrs
      |> Enum.into(%{
        type: "debit",
        amount: Decimal.new("123.1234567890"),
        account_id: account_fixture().id,
        entry_id: entry_fixture().id
      })
      |> Accounting.create_amount()

    amount
  end
end
