# BudgetHub

[![pipeline status](https://gitlab.com/budgethub/core/badges/master/pipeline.svg)](https://gitlab.com/budgethub/core/commits/master)
[![coverage report](https://gitlab.com/budgethub/core/badges/master/coverage.svg)](https://gitlab.com/budgethub/core/commits/master)

## Overview

- [BudgetHub](https://www.budgethub.xyz) is an open source budgeting platform.
- This project provides the BudgetHub API for consumption.
- A public deployment of this code is maintained by [BudgetHub](https://www.budgethub.xyz)

## Why Open Source?

- I started this project as a way for me to learn Elixir and ReactJS.
- I've been a fan of envelope budgeting since 2011 and I always found it a challenge to
share my passion for budgeting with other people because a lot of the best budgeting apps are locked behind a paywall.
- Being open source ensures that anyone who uses this software will know what's going on under the hood:
  - Open Source prevent unethical situations.
  - It also enforces transparency.
  - Security is forced to be a priority.
- Another benefit of being open source is that anyone can contribute and improve [BudgetHub](https://www.budgethub.xyz) for everyone.
- This project was inspired by [OpenDota](https://www.opendota.com/), which is an open source project for viewing
data related to the popular game Dota 2. The reason why it's big and successful is because it's open source.
I want to emulate what they did for this project.
- Honestly, this endeavor is too big for 1 person to do. I want to encourage other developers who are better skilled than I am to help out in this project by going all-in on open source.

## Tech Stack

- Language: [Elixir](https://elixir-lang.org/)
- Web Framework: [Phoenix](http://phoenixframework.org/)
- Database: [PostgreSQL](https://www.postgresql.org/)

## How to contribute

* Fork this repository
* Write your awesome feature/fix/refactor
* Create a pull request
* Wait for your PR to be reviewed and merged
* You're now a contributor!

For more information, check out our [guidelines for contributing](CONTRIBUTING.md).

## Quick Start

```
git clone https://gitlab.com/budgethub/core.git # or clone your own fork
cd core
mix deps.get
mix ecto.create
mix ecto.migrate
mix phx.server
```

Afterwards, open your favorite REST client and go to [localhost:4000/api](http://localhost:4000/api)
