# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 0.1.0 - 2018-10-14

### Added

- This project.
- Development tools and environment setup. [!1](https://gitlab.com/budgethub/core/merge_requests/1)
- Gitlab CI integration. [!2](https://gitlab.com/budgethub/core/merge_requests/2)
- App releases through Distillery and Docker. [!3](https://gitlab.com/budgethub/core/merge_requests/3)
- Continuous Deployment to Kubernetes through Gitlab CI. [!4](https://gitlab.com/budgethub/core/merge_requests/4) [!5](https://gitlab.com/budgethub/core/merge_requests/5)
