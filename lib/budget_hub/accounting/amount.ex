defmodule BudgetHub.Accounting.Amount do
  @moduledoc """
  The Amount module represents debit and credit amounts in the system.
  """

  use Ecto.Schema
  import Ecto.Changeset
  alias BudgetHub.Accounting.{Account, Entry}

  schema "amounts" do
    field(:amount, :decimal)
    field(:type, :string)
    belongs_to(:account, Account)
    belongs_to(:entry, Entry)

    timestamps()
  end

  @doc false
  def changeset(amount, attrs) do
    amount
    |> cast(attrs, [:type, :amount, :account_id, :entry_id])
    |> validate_required([:type, :amount, :account_id, :entry_id])
    |> assoc_constraint(:account)
    |> assoc_constraint(:entry)
    |> validate_inclusion(:type, ["debit", "credit"])
  end
end
