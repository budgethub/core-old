defmodule BudgetHub.Accounting.Entry do
  @moduledoc """
  Entries are the recording of debits and credits to various accounts.
  This table can be thought of as a traditional accounting Journal.

  Posting to a Ledger can be considered to happen automatically, since
  Accounts have the reverse 'has_many' relationship to either it's credit or
  debit entries.

  See: http://en.wikipedia.org/wiki/Journal_entry
  """

  use Ecto.Schema
  import Ecto.Changeset
  alias BudgetHub.Accounting.{Amount, Entry}

  schema "entries" do
    field(:date, :date)
    field(:description, :string)
    has_many(:amounts, Amount)

    timestamps()
  end

  @doc false
  def changeset(%Entry{} = entry, attrs) do
    entry
    |> cast(attrs, [:description, :date])
    |> validate_required([:description, :date])
  end
end
