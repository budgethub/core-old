defmodule BudgetHub.Accounting do
  @moduledoc """
  The Accounting context.
  """

  import Ecto.Query, warn: false
  alias BudgetHub.Repo

  alias BudgetHub.Accounting.Account

  @doc """
  Returns the list of accounts.

  ## Examples

      iex> list_accounts()
      [%Account{}, ...]

  """
  def list_accounts do
    Repo.all(Account)
  end

  @doc """
  Gets a single account.

  Raises `Ecto.NoResultsError` if the Account does not exist.

  ## Examples

      iex> get_account!(123)
      %Account{}

      iex> get_account!(456)
      ** (Ecto.NoResultsError)

  """
  def get_account!(id), do: Repo.get!(Account, id)

  @doc """
  Gets a single account.

  Returns `nil` if the Account does not exist.

  ## Examples

      iex> get_account(123)
      %Account{}

      iex> get_account(456)
      nil

  """
  def get_account(id) do
    case Repo.get(Account, id) do
      %Account{} = account ->
        account

      nil ->
        {:error, :not_found}
    end
  end

  @doc """
  Creates an account.

  ## Examples

      iex> create_account(%{field: value})
      {:ok, %Account{}}

      iex> create_account(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_account(attrs \\ %{}) do
    %Account{}
    |> Account.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates an account.

  ## Examples

      iex> update_account(account, %{field: new_value})
      {:ok, %Account{}}

      iex> update_account(account, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_account(%Account{} = account, attrs) do
    account
    |> Account.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes an Account.

  ## Examples

      iex> delete_account(account)
      {:ok, %Account{}}

      iex> delete_account(account)
      {:error, %Ecto.Changeset{}}

  """
  def delete_account(%Account{} = account) do
    Repo.delete(account)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking account changes.

  ## Examples

      iex> change_account(account)
      %Ecto.Changeset{source: %Account{}}

  """
  def change_account(%Account{} = account) do
    Account.changeset(account, %{})
  end

  alias BudgetHub.Accounting.Entry

  @doc """
  Returns the list of entries.

  ## Examples

      iex> list_entries()
      [%Entry{}, ...]

  """
  def list_entries do
    Repo.all(Entry)
  end

  @doc """
  Gets a single entry.

  Raises `Ecto.NoResultsError` if the Entry does not exist.

  ## Examples

      iex> get_entry!(123)
      %Entry{}

      iex> get_entry!(456)
      ** (Ecto.NoResultsError)

  """
  def get_entry!(id), do: Repo.get!(Entry, id)

  @doc """
  Gets a single entry.

  Returns {:error, :not_found} if the Entry does not exist.

  ## Examples

      iex> get_entry(123)
      %Entry{}

      iex> get_entry(456)
      {:error, :not_found}

  """
  def get_entry(id) do
    case Repo.get(Entry, id) do
      %Entry{} = entry ->
        entry

      nil ->
        {:error, :not_found}
    end
  end

  @doc """
  Creates a entry.

  ## Examples

      iex> create_entry(%{field: value})
      {:ok, %Entry{}}

      iex> create_entry(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_entry(attrs \\ %{}) do
    %Entry{}
    |> Entry.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a entry.

  ## Examples

      iex> update_entry(entry, %{field: new_value})
      {:ok, %Entry{}}

      iex> update_entry(entry, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_entry(%Entry{} = entry, attrs) do
    entry
    |> Entry.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Entry.

  ## Examples

      iex> delete_entry(entry)
      {:ok, %Entry{}}

      iex> delete_entry(entry)
      {:error, %Ecto.Changeset{}}

  """
  def delete_entry(%Entry{} = entry) do
    Repo.delete(entry)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking entry changes.

  ## Examples

      iex> change_entry(entry)
      %Ecto.Changeset{source: %Entry{}}

  """
  def change_entry(%Entry{} = entry) do
    Entry.changeset(entry, %{})
  end

  alias BudgetHub.Accounting.Amount

  @doc """
  Returns the list of amounts.

  ## Examples

      iex> list_amounts()
      [%Amount{}, ...]

  """
  def list_amounts do
    Repo.all(Amount)
  end

  @doc """
  Gets a single amount.

  Raises `Ecto.NoResultsError` if the Amount does not exist.

  ## Examples

      iex> get_amount!(123)
      %Amount{}

      iex> get_amount!(456)
      ** (Ecto.NoResultsError)

  """
  def get_amount!(id), do: Repo.get!(Amount, id)

  @doc """
  Gets a single amount.

  Returns {:error, :not_found} if the Amount does not exist.

  ## Examples

      iex> get_amount(123)
      %Amount{}

      iex> get_amount(456)
      {:error, :not_found}

  """
  def get_amount(id) do
    case Repo.get(Amount, id) do
      %Amount{} = amount ->
        amount

      nil ->
        {:error, :not_found}
    end
  end

  @doc """
  Creates a amount.

  ## Examples

      iex> create_amount(%{field: value})
      {:ok, %Amount{}}

      iex> create_amount(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_amount(attrs \\ %{}) do
    %Amount{}
    |> Amount.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a amount.

  ## Examples

      iex> update_amount(amount, %{field: new_value})
      {:ok, %Amount{}}

      iex> update_amount(amount, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_amount(%Amount{} = amount, attrs) do
    amount
    |> Amount.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Amount.

  ## Examples

      iex> delete_amount(amount)
      {:ok, %Amount{}}

      iex> delete_amount(amount)
      {:error, %Ecto.Changeset{}}

  """
  def delete_amount(%Amount{} = amount) do
    Repo.delete(amount)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking amount changes.

  ## Examples

      iex> change_amount(amount)
      %Ecto.Changeset{source: %Amount{}}

  """
  def change_amount(%Amount{} = amount) do
    Amount.changeset(amount, %{})
  end
end
