defmodule BudgetHub.Accounting.Account do
  @moduledoc """
  The Account module represents accounts in the system.
  Each account must be one of the following types:

  TYPE       | NORMAL BALANCE | DESCRIPTION
  ----------------------------------------------------------------------
  Asset      | Debit          | Resources owned by the Business Entity
  Liability  | Credit         | Debts owed to outsiders
  Equity     | Credit         | Owners rights to the Assets
  Revenue    | Credit         | Increases in owners equity
  Expense    | Debit          | Assets or services consumed in the generation of revenue
  """

  use Ecto.Schema
  import Ecto.Changeset

  alias BudgetHub.Accounting.{Account, Amount}

  schema "accounts" do
    field(:contra, :boolean, default: false)
    field(:name, :string)
    field(:type, :string)
    has_many(:amounts, Amount)

    timestamps()
  end

  @account_types ["asset", "liability", "equity", "revenue", "expense"]

  @doc false
  def changeset(%Account{} = account, attrs) do
    account
    |> cast(attrs, [:name, :type, :contra])
    |> validate_required([:name, :type, :contra])
    |> validate_inclusion(:type, @account_types)
  end
end
