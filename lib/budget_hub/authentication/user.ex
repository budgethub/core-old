defmodule BudgetHub.Authentication.User do
  @moduledoc """
  User schema.
  """

  use Ecto.Schema
  import Ecto.Changeset
  alias Comeonin.Pbkdf2

  schema "users" do
    field(:first_name, :string)
    field(:last_name, :string)
    field(:password, :string, virtual: true)
    field(:password_hash, :string)
    field(:username, :string)

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :first_name, :last_name])
    |> validate_required([:username, :first_name, :last_name])
  end

  @doc false
  def registration_changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :password, :first_name, :last_name])
    |> validate_required([:username, :password, :first_name, :last_name])
    |> unique_constraint(:username)
    |> validate_length(:username, min: 6, max: 20)
    |> validate_length(:password, min: 8, max: 30)
    |> validate_confirmation(:password)
    |> hash_password()
  end

  defp hash_password(%Ecto.Changeset{valid?: true, changes: %{password: pass}} = changeset) do
    put_change(changeset, :password_hash, Pbkdf2.hashpwsalt(pass))
  end

  defp hash_password(changeset), do: changeset
end
