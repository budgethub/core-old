defmodule BudgetHub.Authentication do
  @moduledoc """
  The Authentication context.
  """

  import Ecto.Query, warn: false
  alias BudgetHub.Repo

  alias BudgetHub.Authentication.User
  alias Comeonin.Pbkdf2

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Gets a single user.

  Returns {:error, :not_found} if the User does not exist.

  ## Examples

      iex> get_user(123)
      %User{}

      iex> get_user(456)
      {:error, :not_found}

  """
  def get_user(id) do
    case Repo.get(User, id) do
      %User{} = user ->
        user

      nil ->
        {:error, :not_found}
    end
  end

  @doc """
  Gets a single user by the given attributes.

  Returns {:error, :not_found} if the User does not exist.

  ## Examples

      iex> get_user_by(%{username: "validuser"})
      %User{}

      iex> get_user_by(%{username: "ghostuser"})
      {:error, :not_found}

  """
  def get_user_by(map) when is_map(map) and map_size(map) > 0 do
    case Repo.get_by(User, map) do
      %User{} = user ->
        user

      nil ->
        {:error, :not_found}
    end
  end

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.registration_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end

  @doc """
  Authenticates a user using the given username and password combination

  ## Examples

      iex> authenticate_user(%{"username" => "validuser", "password" => "password"})
      {:ok, %User{}}

      iex> authenticate_user(%{"username" => "invaliduser", "password" => "password"})
      {:error, :not_found}

  """
  def authenticate_user(%{"username" => username, "password" => password}) do
    user = get_user_by(%{username: username})

    if user != {:error, :not_found} && Pbkdf2.checkpw(password, user.password_hash) do
      {:ok, user}
    else
      Pbkdf2.dummy_checkpw()
      {:error, :not_found}
    end
  end
end
