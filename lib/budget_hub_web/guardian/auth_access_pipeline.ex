defmodule BudgetHubWeb.Guardian.AuthAccessPipeline do
  @moduledoc """
  Plug for requiring a request to be authenticated before
  it gets to a controller or action.

  ### Examples

      plug BudgetHubWeb.Guardian.AuthAccessPipeline

      plug BudgetHubWeb.Guardian.AuthAccessPipeline when action in [:index, :show]

      plug BudgetHubWeb.Guardian.AuthAccessPipeline when action not in [:update, :create]
  """

  use Guardian.Plug.Pipeline,
    otp_app: :budget_hub,
    module: BudgetHub.Guardian,
    error_handler: BudgetHubWeb.Guardian.AuthErrorHandler

  plug Guardian.Plug.VerifyHeader, claims: %{"typ" => "access"}, realm: "Bearer"
  plug Guardian.Plug.EnsureAuthenticated
  plug Guardian.Plug.LoadResource, allow_blank: true
end
