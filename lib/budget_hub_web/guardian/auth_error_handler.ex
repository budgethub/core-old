defmodule BudgetHubWeb.Guardian.AuthErrorHandler do
  @moduledoc """
  Module for enabling Guardian to return 401 responses.
  """
  use BudgetHubWeb, :controller

  @doc "Guardian requires the error handler method to be named auth_error"
  def auth_error(conn, _error, _opts) do
    conn
    |> put_status(401)
    |> put_view(BudgetHubWeb.ErrorView)
    |> render("401.json")
    |> halt()
  end
end
