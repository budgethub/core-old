defmodule BudgetHubWeb.Cors do
  @moduledoc """
  Module for defining CORS origin at run time.
  cors_plug defines its origin configuration at compile time so this is the solution.

  See: https://github.com/mschae/cors_plug/issues/42
  """

  def origin do
    if Application.get_env(:budget_hub, :env) == :prod do
      "${CORS_ORIGIN}"
    else
      "*"
    end
  end
end
