defmodule BudgetHubWeb.UserView do
  use BudgetHubWeb, :view
  alias BudgetHubWeb.UserView

  def render("show.json", %{user: user}) do
    render_one(user, UserView, "user.json")
  end

  def render("user.json", %{user: user}) do
    %{
      id: user.id,
      username: user.username,
      first_name: user.first_name,
      last_name: user.last_name
    }
  end
end
