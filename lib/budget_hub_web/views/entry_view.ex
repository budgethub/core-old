defmodule BudgetHubWeb.EntryView do
  use BudgetHubWeb, :view
  alias BudgetHubWeb.EntryView

  def render("index.json", %{entries: entries}) do
    render_many(entries, EntryView, "entry.json")
  end

  def render("show.json", %{entry: entry}) do
    render_one(entry, EntryView, "entry.json")
  end

  def render("entry.json", %{entry: entry}) do
    %{id: entry.id, description: entry.description, date: entry.date}
  end
end
