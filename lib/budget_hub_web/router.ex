defmodule BudgetHubWeb.Router do
  use BudgetHubWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", BudgetHubWeb do
    pipe_through :api

    resources "/accounts", AccountController
    resources "/entries", EntryController, except: [:new, :edit]
    resources "/token", TokenController, only: [:create]
    resources "/users", UserController, only: [:create]
  end
end
