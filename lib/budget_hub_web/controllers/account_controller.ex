defmodule BudgetHubWeb.AccountController do
  use BudgetHubWeb, :controller

  alias BudgetHub.Accounting
  alias BudgetHub.Accounting.Account

  action_fallback BudgetHubWeb.FallbackController

  def index(conn, _params) do
    accounts = Accounting.list_accounts()
    render(conn, "index.json", accounts: accounts)
  end

  def create(conn, account_params) do
    with {:ok, %Account{} = account} <- Accounting.create_account(account_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.account_path(conn, :show, account))
      |> render("show.json", account: account)
    end
  end

  def show(conn, %{"id" => id}) do
    with %Account{} = account <- Accounting.get_account(id) do
      render(conn, "show.json", account: account)

      conn
      |> put_resp_header("location", Routes.account_path(conn, :show, account))
      |> render("show.json", account: account)
    end
  end

  def update(conn, %{"id" => id} = account_params) do
    with %Account{} = account <- Accounting.get_account(id) do
      with {:ok, %Account{} = account} <- Accounting.update_account(account, account_params) do
        render(conn, "show.json", account: account)
      end
    end
  end

  def delete(conn, %{"id" => id}) do
    with %Account{} = account <- Accounting.get_account(id) do
      with {:ok, %Account{}} <- Accounting.delete_account(account) do
        send_resp(conn, :no_content, "")
      end
    end
  end
end
