defmodule BudgetHubWeb.UserController do
  use BudgetHubWeb, :controller

  alias BudgetHub.Authentication
  alias BudgetHub.Authentication.User
  alias BudgetHub.Guardian

  action_fallback BudgetHubWeb.FallbackController

  def create(conn, user_params) do
    with {:ok, %User{} = user} <- Authentication.create_user(user_params) do
      {:ok, token, _claims} = Guardian.encode_and_sign(user)

      conn
      |> put_status(:created)
      |> put_resp_header("authorization", "Bearer #{token}")
      |> render("show.json", user: user)
    end
  end
end
