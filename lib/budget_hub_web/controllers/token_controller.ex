defmodule BudgetHubWeb.TokenController do
  use BudgetHubWeb, :controller

  alias BudgetHub.Authentication
  alias BudgetHub.Authentication.User
  alias BudgetHub.Guardian

  action_fallback BudgetHubWeb.FallbackController

  def create(conn, token_params) do
    with {:ok, %User{} = user} <- Authentication.authenticate_user(token_params) do
      {:ok, token, _claims} = Guardian.encode_and_sign(user)

      conn
      |> put_status(:created)
      |> put_resp_header("authorization", "Bearer #{token}")
      |> put_view(BudgetHubWeb.UserView)
      |> render("show.json", user: user)
    end
  end
end
