defmodule BudgetHubWeb.EntryController do
  use BudgetHubWeb, :controller

  alias BudgetHub.Accounting
  alias BudgetHub.Accounting.Entry

  action_fallback BudgetHubWeb.FallbackController

  def index(conn, _params) do
    entries = Accounting.list_entries()
    render(conn, "index.json", entries: entries)
  end

  def create(conn, entry_params) do
    with {:ok, %Entry{} = entry} <- Accounting.create_entry(entry_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.entry_path(conn, :show, entry))
      |> render("show.json", entry: entry)
    end
  end

  def show(conn, %{"id" => id}) do
    with %Entry{} = entry <- Accounting.get_entry(id) do
      render(conn, "show.json", entry: entry)

      conn
      |> put_resp_header("location", Routes.entry_path(conn, :show, entry))
      |> render("show.json", entry: entry)
    end
  end

  def update(conn, %{"id" => id} = entry_params) do
    with %Entry{} = entry <- Accounting.get_entry(id) do
      with {:ok, %Entry{} = entry} <- Accounting.update_entry(entry, entry_params) do
        render(conn, "show.json", entry: entry)
      end
    end
  end

  def delete(conn, %{"id" => id}) do
    with %Entry{} = entry <- Accounting.get_entry(id) do
      with {:ok, %Entry{}} <- Accounting.delete_entry(entry) do
        send_resp(conn, :no_content, "")
      end
    end
  end
end
