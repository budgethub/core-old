FROM alpine:latest
COPY _build/prod/rel/budget_hub /opt/app
RUN apk update && apk --no-cache --update add bash openssl-dev
ENV PORT=8080 MIX_ENV=prod REPLACE_OS_VARS=true
WORKDIR /opt/app
EXPOSE ${PORT}
CMD ["/opt/app/bin/budget_hub", "foreground"]
